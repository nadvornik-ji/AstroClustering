#!/bin/sh
SCHEMA="bextract_jul15"
RES_DIR="/var/gavo/inputs/extract_jul15"
ANTARES_CSV_PATH="/data/OSPS/extract_csv"
CLUSTER_PATH="/root/astro-clustering"
BIGBANG_CSV_PATH="/root/lightcurve_run/dataCSV"
DB_NAME="gavo"
DB_USERNAME="gavoadmin"
	
ssh antares "
	cd $RES_DIR;
	gavo imp -cL q import;
	psql $DB_NAME -U $DB_USERNAME \\
		-c \"COPY (select raj2000, dej2000, obsname_id,\\
					starno from $SCHEMA.observation) \\
				TO STDOUT WITH CSV HEADER \" > \\
	$ANTARES_CSV_PATH/${SCHEMA}_sources.csv
"

cd $CLUSTER_PATH;
./cluster -s incremental \
	-i $BIGBANG_CSV_PATH/${SCHEMA}_sources \
	-c $BIGBANG_CSV_PATH/${SCHEMA}_catalog.csv \
	-r $BIGBANG_CSV_PATH/${SCHEMA}_relations.csv \
	-t 16

ssh antares "
	cd $RES_DIR;
	gavo imp q create_catalog;
	cat dataCSV/${SCHEMA}_catalog.csv | \\
		psql $DB_NAME -U $DB_USERNAME \\
			-c \"COPY $SCHEMA.objcat (cat_id, raj2000, dej2000) \\
				FROM STDIN CSV HEADER\";
	cat dataCSV/${SCHEMA}_relations.csv | \\
		psql $DB_NAME -U $DB_USERNAME \\
			-c \"COPY $SCHEMA.observation_objcat (cat_id, obsname_id, starNo) \\
				FROM STDIN CSV HEADER\";
	gavo imp q make_denormalization;
	gavo imp q merge;
	gavo imp q make_ssap;
"