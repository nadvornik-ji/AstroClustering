#Path to the location of the Healpix C++ build products
#Note that this is typically <...>/cxx/<something>/
HEALPIXPATH=/usr/local/Healpix_3.20/src/cxx/basic_gcc
ARMADILLOPATH=/usr/local/armadillo-5.000.1
INC_HEAL_DIR=$(HEALPIXPATH)/include 
LIB_HEAL_DIR=$(HEALPIXPATH)/lib 
INC_ARMA_DIR=$(ARMADILLOPATH)/include
LIB_DIR=./lib
INC_KM_DIR=$(LIB_DIR)/kmlocal
LIB_KM_DIR=$(LIB_DIR)/kmlocal
INC_TCLAP_DIR=$(LIB_DIR)/tclap
INC_FASTCLUSTER_DIR=$(LIB_DIR)/fastcluster
GTEST_DIR=$(LIB_DIR)/gtest-1.7.0
INC_GTEST_DIR=$(GTEST_DIR)/include
CC=g++
CCFLAGS= -I$(INC_HEAL_DIR) -L$(LIB_HEAL_DIR)  -I$(INC_ARMA_DIR) -I$(INC_KM_DIR) -L$(LIB_KM_DIR) -lkmlocal -g \
	-I$(INC_TCLAP_DIR) -I$(INC_FASTCLUSTER_DIR) -O0 -W -Wall -std=c++11 -DARMA_DONT_USE_WRAPPER
TEST_RUNNER=./test_all

LIBS=-lhealpix_cxx -lcxxsupport -fopenmp -lboost_system -lboost_chrono \
    -lboost_program_options -lblas -llapack 

cluster: src/ClusterMain.cpp  src/Config.cpp \
	src/io/CsvOperator.cpp  \
	src/model/Coordinate.cpp \
	src/helpers/HealPixHelper.cpp \
	src/helpers/StreamOverloads.cpp \
	src/core/ChunkOperator.cpp \
	src/core/ClusteringTask.cpp \
	src/core/strategy/ClusteringStrategy.cpp \
	src/core/strategy/IncrementalStrategy.cpp \
	src/core/strategy/kmeans/KmeansStrategy.cpp \
	src/core/strategy/kmeans/KmeansLloyds.cpp \
	src/core/strategy/kmeans/KmeansSwap.cpp \
	src/core/strategy/kmeans/KmeansEZHybrid.cpp \
	src/core/strategy/kmeans/KmeansHybrid.cpp \
	src/core/strategy/EMStrategy.cpp \
	src/core/ClusteringController.cpp 
	
	$(CC) -o $@ $^ $(CCFLAGS) $(LIBS)
	
# build tests
build-tests: .build-tests-post

.build-tests-pre:
# Add your pre 'build-tests' code here...

.build-tests-post: .build-tests-impl
# Add your post 'build-tests' code here...

TEST_LIST=testChunkOperator.cpp

.build-tests-impl: $(GTEST_DIR)/src/gtest-all.cc \
	    $(GTEST_DIR)/src/gtest_main.cc $(TEST_LIST) 
	$(CC) -o $(TEST_RUNNER) $^ -g -Wall -Wextra -pthread \
	    -I$(GTEST_DIR) -isystem $(INC_GTEST_DIR) 
            

# run tests
test: 
	$(TEST_RUNNER)

.test-pre:
# Add your pre 'test' code here...

.test-post: .test-impl
# Add your post 'test' code here...

.test-impl:

.PHONY: clean

clean:
	rm -f *~ cluster

	


