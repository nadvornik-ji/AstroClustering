/* 
 * File:   ClusterMain.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 3, 2015, 7:36 PM
 */
#include <boost/program_options.hpp>
#include <iostream>
#include <sstream>

#include "core/ClusteringController.h"
#include "Config.h"

using namespace std;
namespace po = boost::program_options;

/**
 * Function for parsing command line arguments. Will take argc and argv and
 * return the parsed parameters needed by our program in the ouptu parameters.
 * 
 * @param argc
 * @param argv
 * @param strategy The strategy which to use for clustering
 * @param csvPath Path to the CSV input.
 * @param catalogPath Path to the catalog CSV output.
 * @param relationsPath Path to the relations CSV output
 * @param timeLogPath Path to the timing logFile
 * @param taskSize The size of one parallel task of clustering
 * @param noThreads Number of threads to run
 * @param varFloor Variance floor used for EM algorithms
 * @return Is input valid?
 */
bool process_command_line(int argc, char** argv,
        string * strategy,
        string * csvPath,
        string * catalogPath,
        string * relationsPath,
        string * timeLogPath,
        int * taskSize,
        int * noThreads,
        double * varFloor) {
    try {
        po::options_description general("General options", 1024, 512);
        general.add_options()
                ("help,h", "produce help message")
                ("help-module", po::value<string>()->implicit_value("all"),
                "produce a help for a given module")
                ("strategy,s", po::value<string>(strategy)->required(),
                "set the strategy")
                ("input-path,i", po::value<string>(csvPath)->required(),
                "path to the input CSV")
                ("catalog-path,c", po::value<string>(catalogPath)->required(),
                "path to the output catalog CSV")
                ("relations-path,r", po::value<string>(relationsPath)->required(),
                "path to the output relations CSV")
                ("time-log-path", po::value<string>(timeLogPath),
                "path to the timings output")
                ;
        po::options_description parallel("Parallelization options", 1024, 512);
        parallel.add_options()
                ("task-size", po::value<int>(taskSize),
                "set the task size for parallel processing")
                ("no-threads,t", po::value<int>(noThreads),
                "set the task size for parallel processing")
                ;
        po::options_description em("Expectation maximisation options", 1024, 512);
        em.add_options()
                ("var-floor", po::value<double>(varFloor),
                "set the var-floor for mahalanobis distance computing")
                ;

        // Declare an options description instance which will include
        // all the options
        po::options_description all("Allowed options");
        all.add(general).add(parallel).add(em);

        // Declare an options description instance which will be shown
        // to the user
        po::options_description visible("Allowed options");
        visible.add(general);

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, all), vm);

        if (vm.count("help")) {
            cout << visible;
            return 0;
        }
        if (vm.count("help-module")) {
            const string& s = vm["help-module"].as<string>();
            if (s == "parallel") {
                cout << parallel;
            } else if (s == "all") {
                cout << all;
            } else if (s == "em") {
                cout << em;
            } else {
                cout << "Unknown module '"
                        << s << "' in the --help-module option\n";
                return 1;
            }
            return 0;
        }

        // There must be an easy way to handle the relationship between the
        // option "help" and "host"-"port"-"config"
        // Yes, the magic is putting the po::notify after "help" option check
        po::notify(vm);
    } catch (exception& e) {
        cerr << "Error: " << e.what() << "\n";
        return false;
    } catch (...) {
        cerr << "Unknown error!" << "\n";
        return false;
    }

    return true;
}

/**
 * Main function only parses the arguments and, loads config and creates an
 * instance of ClusteringController and runs it
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char** argv) {
    Config::initialize();
    string strategy, csvPath, catalogPath, relationsPath, timeLogPath;
    int bigPixelNsideExp = -1;
    int noThreads = 1;
    double varFloor = -1;

    bool isInputValid = process_command_line(argc, argv, &strategy, &csvPath,
            &catalogPath, &relationsPath, &timeLogPath, &bigPixelNsideExp,
            &noThreads, &varFloor);
    if (!isInputValid)
        return 1;

    if (bigPixelNsideExp != -1) {
        Config::bigPixelNside = 1 << bigPixelNsideExp;
    }
    if (varFloor != -1) {
        Config::varFloor = varFloor;
    }
    if (timeLogPath.size() != 0) {
        Config::timeLogPath = timeLogPath;
    }


    ClusteringController controller = ClusteringController(strategy, noThreads, csvPath,
            catalogPath, relationsPath);

    return controller.run();
}

