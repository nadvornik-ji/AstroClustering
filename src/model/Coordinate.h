/* 
 * File:   Coordinate.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 3, 2015, 8:37 PM
 */

#ifndef COORDINATE_H
#define	COORDINATE_H
#include "healpix_base.h"

using namespace std;

/**
 * Struct used for storing coordinates in our program. Apart from ra, dec, it
 * has the imageID and starID identifiers to track it back to the original
 * identified source on an image (used when importing the results back into DB).
 * the pragma macro is used for not packing this structure and saving space in 
 * memory.
 */
#pragma pack(push, 1)

struct Coordinate {
    double ra, dec;
    int imageID, starID;

    Coordinate(double ra, double dec, int imageID, int starID) :
    ra(ra), dec(dec), imageID(imageID), starID(starID) {
    }

    /**
     * Copy constructor converting a HEALPix vector representing sky coordinates.
     * @param v HEALPix vector
     */
    Coordinate(vec3 v);
    Coordinate(const Coordinate & c);
    ~Coordinate();

    bool operator==(const Coordinate &c) const;
    bool operator!=(const Coordinate &c) const;

    /**
     * Method comparing another coordinate which went through some computation
     * and can contain rounding errors.
     * @param c Another coordinate
     * @return 
     */
    bool isAlmostSame(const Coordinate &c) const;

    /**
     * Generates a HEALPix Pointing structure representing current coordinates.
     * @return 
     */
    pointing getHealPixPointing() const;

    /**
     * Generates HEALPix vector from our coordinates.
     * @return 
     */
    vec3 get3DVector() const;
};
#pragma pack(pop)

ostream& operator<<(ostream &strm, const Coordinate &c);
#endif	/* COORDINATE_H */

