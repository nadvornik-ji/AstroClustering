/* 
 * File:   Coordinate.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 3, 2015, 8:39 PM
 */

#include <math.h>
#include <iomanip>
#include <iostream>
#include "Coordinate.h"
#include "healpix_base.h"

Coordinate::Coordinate(const Coordinate &c) {
    this->ra = c.ra;
    this->dec = c.dec;
    this->imageID = c.imageID; //these are IDs, no copying!
    this->starID = c.starID;
}

Coordinate::Coordinate(vec3 v) {
    pointing p = pointing(v);
    
    this->ra = (p.phi * 180) / M_PI;
    this->dec = 90 - ((p.theta * 180) / M_PI);
    this->imageID = -1;
    this->starID = -1;
}

Coordinate::~Coordinate() {
}

bool Coordinate::operator==(const Coordinate &c) const {
    if (this->imageID == -1) {
        return (this->ra == c.ra && this->dec == c.dec);
    } else {
        return (this->imageID == c.imageID && this->starID == c.starID);
    }
}

bool Coordinate::isAlmostSame(const Coordinate &c) const {
    return (fabs(c.ra - this->ra) < numeric_limits<double>::epsilon() &&
            fabs(c.dec - this->dec) < numeric_limits<double>::epsilon());
}

bool Coordinate::operator!=(const Coordinate &c) const {
    return !(*this == c);
}

pointing Coordinate::getHealPixPointing() const {
    pointing healPixPointing = pointing();
    // colatitude in radian, some function of observations_degrees[i]
    healPixPointing.theta = (90 - this->dec) * M_PI / 180; 
    healPixPointing.phi = this->ra * M_PI / 180;
    return healPixPointing;
}

vec3 Coordinate::get3DVector() const {
    pointing currMyPointing = getHealPixPointing();
    return currMyPointing.to_vec3();
}

std::ostream& operator<<(std::ostream &strm, const Coordinate &c) {
    return strm << std::setprecision(15) << "Coordinate ID: " << 
            c.imageID << c.starID << "(" << c.ra << " " << c.dec << ")";
}
