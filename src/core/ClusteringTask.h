/* 
 * File:   ClusteringTask.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 12, 2015, 12:28 PM
 */

#ifndef CLUSTERINGTASK_H
#define	CLUSTERINGTASK_H
#include "healpix_base.h"
#include "strategy/ClusteringStrategy.h"
#include "../helpers/ClusteringTypes.h"

using namespace std;
/**
 * Class representing an individual task to be computed by clustering strategy.
 * It always represents an area on the sky for which we are processing the 
 * sources. It is defined by a HEALPix pixel where the sources lie.
 * 
 * I responsible for remembering the strategy and using it when somebody calls 
 * the cluster method of this class. These tasks are usually computed in
 * parallel by the ClusteringController.
 * 
 * The task does not hold the copies Coordinate structures itself, it just holds 
 * pointers to these structures.
 * 
 * @param taskID Identifier of a task, this is a HEALPix pixel ID.
 * @param strategyStr Strategy which should be used to cluster the data.
 * @param obsInCurrCell Observations in the current HEALPix pixel (from which 
 * this task was created)
 * @param obsInCurrOverlap Observations in the neighboring region of this pixel.
 * How big area it is is defined in the task_size argument of our program.
 */
class ClusteringTask {
public:

    ClusteringTask(int64 taskID, string strategyStr,
            vector<Coordinate *> *obsInCurrCell, 
            vector<Coordinate *> *obsInCurrOverlap);
    ~ClusteringTask();

    cluster_map *getResult() {
        return &result;
    }

    int64 getTaskID() const {
        return taskID;
    }

    vector<Coordinate *>* getObsInOverlap() const {
        return obsInOverlap;
    }

    vector<Coordinate *>* getObsInCell() const {
        return obsInCell;
    }
    
    /**
     * Method which runs the clustering algorithm on this individual task. This
     * method is always run by a single thread.
     */
    void cluster();

private:
    vector<Coordinate *> *obsInCell, *obsInOverlap;
    int64 taskID;
    ClusteringStrategy *strategy;
    cluster_map result;
};



#endif	/* CLUSTERINGTASK_H */

