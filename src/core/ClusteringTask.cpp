/* 
 * File:   ClusteringTask.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 12, 2015, 12:29 PM
 */

#include <math.h>

#include "ChunkOperator.h"
#include "../io/CsvOperator.h"
#include "ClusteringTask.h"
#include "../helpers/ClusteringTypes.h"
#include "strategy/IncrementalStrategy.h"
#include "strategy/kmeans/KmeansStrategy.h"

using namespace std;
#include "../helpers/ClusteringTypes.h"
#include "strategy/EMStrategy.h"
#include "strategy/kmeans/KmeansLloyds.h"
#include "strategy/kmeans/KmeansSwap.h"
#include "strategy/kmeans/KmeansEZHybrid.h"
#include "strategy/kmeans/KmeansHybrid.h"

ClusteringTask::ClusteringTask(int64 taskID, string strategyStr,
        vector<Coordinate *>*obsInCell,
        vector<Coordinate *>*obsInOverlap) {
    this->taskID = taskID;
    this->obsInCell = obsInCell;
    this->obsInOverlap = obsInOverlap;
    if (strategyStr == "incremental") {
        this->strategy = new IncrementalStrategy(obsInCell,
                obsInOverlap);
    } else if (strategyStr == "kmeans-lloyd") {
        this->strategy = new KmeansLloyds(obsInCell, obsInOverlap);
    } else if (strategyStr == "kmeans-swap") {
        this->strategy = new KmeansSwap(obsInCell, obsInOverlap);
    } else if (strategyStr == "kmeans-ez-hybrid") {
        this->strategy = new KmeansEZHybrid(obsInCell, obsInOverlap);
    } else if (strategyStr == "kmeans-hybrid") {
        this->strategy = new KmeansHybrid(obsInCell, obsInOverlap);
    } else if (strategyStr == "em") {
        this->strategy = new EMStrategy(obsInCell, obsInOverlap);
    } else {
        cerr << "Unsupported strategy option." << endl;
        throw new runtime_error("Unsupported strategy option.");
    }
}

ClusteringTask::~ClusteringTask() {
    delete this->strategy;
}

/**
 * The clustering task itself will take care of the result, so we don't return
 * it anywhere yet.
 */
void ClusteringTask::cluster() {
    this->strategy->cluster(&this->result);
}





