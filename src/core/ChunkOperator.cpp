/* 
 * File:   ChunkOperator.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 10, 2015, 12:33 PM
 */

#include <math.h>

#include "ChunkOperator.h"
#include "../io/CsvOperator.h"
#include "ClusteringTask.h"
#include "../helpers/ClusteringTypes.h"

using namespace std;

ChunkOperator::ChunkOperator(coords_list *observations_degrees) {
    this->obsCoords = observations_degrees;
    this->obsInCell = new coords_by_pix();
    this->obsInOverlap = new coords_by_pix();
}

ChunkOperator::~ChunkOperator() {
}

coords_by_pix *ChunkOperator::getObservationsInOverlap() const {
    return obsInOverlap;
}

coords_by_pix *ChunkOperator::getObservationsInCell() const {
    return obsInCell;
}

coords_list *ChunkOperator::getObservationCoordinates() const {
    return obsCoords;
}

int ChunkOperator::buildChunksFromCoordinates(int64 base1Nside, int64 base2Nside) {

    // the algorithm needs a vector of pointings to work, so we need to do some conversion
    vector<pointing> observations;
    for (size_t i = 0; i < obsCoords->size(); i++) {
        observations.push_back(pointing());
        observations[i].theta = (90 - (*obsCoords)[i].dec) * M_PI / 180; // colatitude in radian, some function of observations_degrees[i]
        observations[i].phi = (*obsCoords)[i].ra * M_PI / 180; // longitude in radian, some function of observations_degrees[i]
    }
    size_t noObservations = obsCoords->size();
    Healpix_Base2 base1(base1Nside, NEST, SET_NSIDE);
    Healpix_Base2 base2(base2Nside, NEST, SET_NSIDE);

    for (size_t i = 0; i < noObservations; i++) {
        // first see into which job the observation falls
        int64 idx_lores = base1.ang2pix(observations[i]);
        (*obsInCell)[idx_lores].push_back(&(*obsCoords)[i]);
        coords_by_pix_it got = obsInOverlap->find(idx_lores);
        if (got == obsInOverlap->end()){
            (*obsInOverlap)[idx_lores] = vector<Coordinate *> ();
        }

        // now check whether the surroundings of the observation touch neighbouring jobs
        int64 idx_hires = base2.ang2pix(observations[i]);

        fix_arr<int64, 8> neighbors;
        base2.neighbors(idx_hires, neighbors);
        for (size_t j = 0; j < 8; ++j) {
            if (neighbors[j] >= 0) {
                int64 nbidx_lores = base1.ang2pix(base2.pix2ang(neighbors[j]));

                if (nbidx_lores != idx_lores) { // touches a neighbour cell  
                    if ((*obsInOverlap)[nbidx_lores].empty() ||
                            (*obsInOverlap)[nbidx_lores].back() != &(*obsCoords)[i]) {
                        (*obsInOverlap)[nbidx_lores].push_back(&(*obsCoords)[i]);
                    }
                }
            }
        }
    }
    return 0;
}


