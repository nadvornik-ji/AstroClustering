/* 
 * File:   ClusteringController.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 12, 2015, 5:36 PM
 */
#include "ClusteringController.h"

#include <cstdlib>
#include <vector>
#include <memory>
#include <set>



#include "../io/CsvOperator.h"
#include "ChunkOperator.h"
#include "ClusteringTask.h"
#include "../Config.h"
#include "../helpers/ClusteringTypes.h"
#include "../helpers/HealPixHelper.h"
#include "../helpers/StreamOverloads.h"




ClusteringController::ClusteringController(string strategy, int noThreads,
        string csvPath, string catalogPath, string relationsPath) {
    this->strategyStr = strategy;
    this->csvPath = csvPath;
    this->catalogPath = catalogPath;
    this->relationsPath = relationsPath;
    this->noThreads = noThreads;

}

ClusteringController::~ClusteringController() {
}

int ClusteringController::run() {
#ifdef LOG_TIME  
    ofstream timeLogFile(Config::timeLogPath);
    auto startUserTime = boost::chrono::process_user_cpu_clock::now();
    auto startSystemTime = boost::chrono::process_system_cpu_clock::now();
    auto startRealTime = boost::chrono::process_real_cpu_clock::now();
#endif  
    coords_list *obsCoords; 
    csvOperator = new CsvOperator(csvPath, catalogPath, relationsPath);
    obsCoords = csvOperator->readCoordinates();

    chunkOperator = new ChunkOperator(obsCoords);
#ifdef LOG_TIME     
    auto userTime1 = boost::chrono::process_user_cpu_clock::now();
    auto systemTime1 = boost::chrono::process_system_cpu_clock::now();
    auto realTime1 = boost::chrono::process_real_cpu_clock::now();
    cout << currentDateTime() << " - CSV file read in milliseconds: " << endl;
    logTimes(timeLogFile, userTime1 - startUserTime,
            systemTime1 - startSystemTime, realTime1 - startRealTime);
#endif 
    chunkOperator->buildChunksFromCoordinates(Config::bigPixelNside,
            Config::overlapPixelNside);

    //observations in HEALPix cells
    coords_by_pix *obsInCell = chunkOperator->getObservationsInCell();
    //observations in HEALPix overlaps
    coords_by_pix *obsInOverlap = chunkOperator->getObservationsInOverlap();
    createTasks(obsInCell, obsInOverlap);
#ifdef DEBUG
    for (size_t i = 0; i < clusteringTasks.size(); i++) {
        ClusteringTask *currTask = clusteringTasks[i];
        cout << *currTask << endl;
    }
#endif
#ifdef LOG_TIME
    auto userTime2 = boost::chrono::process_user_cpu_clock::now();
    auto systemTime2 = boost::chrono::process_system_cpu_clock::now();
    auto realTime2 = boost::chrono::process_real_cpu_clock::now();
    cout << currentDateTime() << " - Create Tasks Time in milliseconds: " << endl;
    logTimes(timeLogFile, userTime2 - userTime1,
            systemTime2 - systemTime1, realTime2 - realTime1);
#endif
    cout << currentDateTime() << " - Processing " << clusteringTasks.size() << 
            " tasks." << endl;
    clusterTasks(obsInCell, obsInOverlap);
#ifdef LOG_TIME
    userTime1 = boost::chrono::process_user_cpu_clock::now();
    systemTime1 = boost::chrono::process_system_cpu_clock::now();
    realTime1 = boost::chrono::process_real_cpu_clock::now();
    cout << currentDateTime() << " - Cluster Tasks time in milliseconds: " << endl;
    logTimes(timeLogFile, userTime1 - userTime2,
            systemTime1 - systemTime2, realTime1 - realTime2);
#endif
    collectResults();
#ifdef LOG_TIME   
    userTime2 = boost::chrono::process_user_cpu_clock::now();
    systemTime2 = boost::chrono::process_system_cpu_clock::now();
    realTime2 = boost::chrono::process_real_cpu_clock::now();
    cout << currentDateTime() << " - Collect results time in milliseconds: " << endl;
    logTimes(timeLogFile, userTime2 - userTime1,
            systemTime2 - systemTime1, realTime2 - realTime1);
#endif

#ifdef DEBUG
    cout << "result before writing: " << result << endl;
#endif
    csvOperator->writeCatalog(result);
    csvOperator->writeRelations(result);

#ifdef LOG_TIME
    userTime1 = boost::chrono::process_user_cpu_clock::now();
    systemTime1 = boost::chrono::process_system_cpu_clock::now();
    realTime1 = boost::chrono::process_real_cpu_clock::now();
    cout << currentDateTime() << " - Writing results time in milliseconds: " << endl;
    logTimes(timeLogFile, userTime1 - userTime2,
            systemTime1 - systemTime2, realTime1 - realTime2);
    cout << currentDateTime() << " - Total time in milliseconds: " << endl;
    logTimes(timeLogFile, userTime1 - startUserTime,
            systemTime1 - startSystemTime, realTime1 - startRealTime);
    timeLogFile.close();
#endif

    freeMemory(obsCoords, obsInCell, obsInOverlap);
    return 0;
}

void ClusteringController::collectResults() {
    task_index taskIndex; //just pointers to clusterIDs

    for (size_t i = 0; i < clusteringTasks.size(); i++) {
        int64 currTaskID = clusteringTasks[i]->getTaskID();
        cluster_map *currTaskClusters = clusteringTasks[i]->getResult();

        for (cluster_map_it it = currTaskClusters->begin(); it != currTaskClusters->end(); it++) {
            Coordinate *currClusterID = it->first;
            coords_pointer_list *currClusterMembers = &((*it).second);

#ifdef DEBUG_COLLECT
            cout << "\nProcessing cluster: " << *currClusterID << endl;
#endif

            bool clusterUpdated = false;
            clusterUpdated = checkAndUpdateDuplicateCluster(taskIndex, &result, currTaskID,
                    *currClusterID, *currClusterMembers);
            if (!clusterUpdated) {
#ifdef DEBUG_COLLECT
                cout << "Cluster does not have duplicate, inserting" << endl;
#endif          
                result[currClusterID] = new set<Coordinate *>(
                        currClusterMembers->begin(), currClusterMembers->end());
                /*for (size_t i = 0; i < currClusterMembers->size(); i++) {

                    result[currClusterID]->insert((*currClusterMembers)[i]); //the whole result
                    (*currTaskMap)[currClusterID]->insert((*currClusterMembers)[i]); //result for this task
                }*/
                task_index_it gotTask = taskIndex.find(currTaskID);
                if (gotTask == taskIndex.end()) {
                    taskIndex[currTaskID] = new coords_pointer_list();
                }
                taskIndex[currTaskID]->push_back(currClusterID);
            } else {
                delete currClusterID;
            }
        }
#ifdef DEBUG_COLLECT
        cout << "\nafter iteration: " << i << endl;
        cout << "taskIndex of size: " << taskIndex.size() << endl << taskIndex << endl;
        cout << "result of size: " << result.size() << endl << result << endl;
#endif   
        delete clusteringTasks[i];
    }

    for (task_index_it taskIt = taskIndex.begin(); taskIt != taskIndex.end(); taskIt++) {
        delete taskIt->second;
    }
}

bool ClusteringController::checkAndUpdateDuplicateCluster(
        const task_index & taskIndex,
        cluster_distinct_map * const result,
        int64 currTaskID,
        const Coordinate & currClusterID,
        const coords_pointer_list & currClusterMembers) {

    

    coords_pointer_list myNeighboringTaskClusterIDs = getNeighboringClusters(
            currTaskID, taskIndex);
    double minDistance = Config::clusterDuplicatesJoinRad;
    Coordinate * minDistanceNeighborID = NULL;
    for (coords_list_it it = myNeighboringTaskClusterIDs.begin(); 
            it != myNeighboringTaskClusterIDs.end(); it++) {
        Coordinate * currNeighborClusterID = *it;

        if (currClusterID.isAlmostSame(*currNeighborClusterID)) {
            return true; //we are exactly the same, no need for merging
        }
        double distance = HealPixHelper::computeDistance(currClusterID, *currNeighborClusterID);
        if (distance < Config::clusterDuplicatesJoinRad) {
#ifdef DEBUG_COLLECT
            cout << "found a duplicate." << *currNeighborClusterID <<
                    "at distance " << setprecision(16) << distance << endl;
            cout << "minDistance: " << setprecision(16) << minDistance << endl;
#endif
            if (distance < minDistance) {
#ifdef DEBUG_COLLECT
                cout << "It is the closest one." << endl;
#endif
                minDistance = distance;
                minDistanceNeighborID = currNeighborClusterID;
            } else {
#ifdef DEBUG_COLLECT
                cout << "It is not the closest one." << endl;
#endif                
            }
        }
    }

    if (minDistanceNeighborID != NULL) {
#ifdef DEBUG_COLLECT
        cout << "found a duplicate, merging (with the closest one)." << endl;
#endif
        cluster_distinct_map_it gotResult = result->find(minDistanceNeighborID);
        set<Coordinate *> * currNeighborMembers = gotResult->second;
        mergeFirstClusterIntoSecond(currClusterID, currClusterMembers,
                minDistanceNeighborID, currNeighborMembers);
#ifdef DEBUG_COLLECT
        cout << "second cluster is: " << *minDistanceNeighborID << " with size: "
                << currClusterMembers->size() << endl;
#endif
        return true;
    }
    return false;
}

coords_pointer_list ClusteringController::getNeighboringClusters(
        int64 currTaskID,
        const task_index & taskIndex) {
    Healpix_Base2 base1(Config::bigPixelNside, NEST, SET_NSIDE);
    fix_arr<int64, 8> neighboringTaskIDs;
    coords_pointer_list myNeighboringTaskClusterIDs = coords_pointer_list();
    
    base1.neighbors(currTaskID, neighboringTaskIDs);
    task_index_const_it gotTask = taskIndex.find(currTaskID);

    if (gotTask != taskIndex.end()) {
        myNeighboringTaskClusterIDs = coords_pointer_list(*gotTask->second);
#ifdef DEBUG_COLLECT
        cout << "found myNeighboringTaskClusterIDs: " << myNeighboringTaskClusterIDs.size() << endl;
#endif
    }

    // we need to add also neighboring HealPixels, not only mine.
    for (int i = 0; i < 8; i++) {
        /*bool alreadyVisited = false;
        if (neighboringTaskIDs[i] == currTaskID) {
            alreadyVisited = true; // do not add me again
            cout << "shit happens" << endl;
        }
        for (int j = 0; j < i; j++) {
            if (neighboringTaskIDs[j] == neighboringTaskIDs[i]) {
                cout << "shit happens" << endl;
                alreadyVisited = true; //already visited this neighbor
            }
        }
        if (alreadyVisited) {
            continue;
        }*/
        gotTask = taskIndex.find(neighboringTaskIDs[i]);
        if (gotTask != taskIndex.end()) {
            coords_pointer_list *currNeigborTaskClusterIDs = gotTask->second;
#ifdef DEBUG_COLLECT
            cout << "found currNeighborTaskClusterIDs: " << currNeigborTaskClusterIDs->size() << endl;
#endif
            for (coords_list_it it = currNeigborTaskClusterIDs->begin(); it != currNeigborTaskClusterIDs->end(); it++) {
                Coordinate *clusterID = *it;

                myNeighboringTaskClusterIDs.push_back(clusterID);
            }
        }
    }
#ifdef DEBUG_COLLECT
    cout << "myNeighboringTaskResults found by my neighbors: " << myNeighboringTaskClusterIDs;
#endif
    return myNeighboringTaskClusterIDs;
}

void ClusteringController::mergeFirstClusterIntoSecond(
        const Coordinate & firstClusterID,
        const coords_pointer_list & firstClusterMembers,
        Coordinate * const secondClusterID,
        set<Coordinate *> * const secondClusterMembers) {
#ifdef DEBUG_COLLECT
    cout << "joining clusterID " << *firstClusterID << " with " <<
            firstClusterMembers->size() << " members" <<
            " into clusterID " << *secondClusterID << " with " <<
            secondClusterMembers->size() << " members " << endl;
#endif

    secondClusterID->ra = (secondClusterID->ra * secondClusterMembers->size() +
            firstClusterID.ra * firstClusterMembers.size()) /
            (secondClusterMembers->size() + firstClusterMembers.size());
    secondClusterID->dec = (secondClusterID->dec * secondClusterMembers->size() +
            firstClusterID.dec * firstClusterMembers.size()) /
            (secondClusterMembers->size() + firstClusterMembers.size());

    for (size_t i = 0; i < firstClusterMembers.size(); i++) {
        secondClusterMembers->insert(firstClusterMembers[i]);
    }
}

void ClusteringController::createTasks(coords_by_pix * const obsInCell, 
        coords_by_pix  * const obsInOverlap) {

    for (coords_by_pix_it it = obsInCell->begin(); 
            it != obsInCell->end(); it++) {
        int64 taskID = it->first;
        vector<Coordinate *> *obsInCurrCell = &((*it).second);
        vector<Coordinate *> *obsInCurrOverlap;
        if (obsInOverlap->find(taskID) != obsInOverlap->end()) {
            obsInCurrOverlap = &((*obsInOverlap->find(taskID)).second);
        } else {
            cerr << "this should not happend" << endl;
            exit(1);
        }
        ClusteringTask *newValue = new ClusteringTask(taskID, strategyStr, obsInCurrCell, obsInCurrOverlap);
        clusteringTasks.push_back(newValue);
    }
}

void ClusteringController::clusterTasks(coords_by_pix * const obsInCell, 
        coords_by_pix * const obsInOverlap) {
    omp_set_dynamic(0); // Explicitly disable dynamic teams
    omp_set_num_threads(this->noThreads); // Use 4 threads for all consecutive parallel regions
#pragma omp parallel for schedule(dynamic,1) if (this->noThreads > 1)
    for (size_t i = 0; i < clusteringTasks.size(); i++) {
#ifdef STDOUT_VERBOSE
        cout << "Processing task " << i << "/" << clusteringTasks.size() << endl;
        cout << *clusteringTasks[i];
#endif
/*        if (((int) ((clusteringTasks.size() / ((float) 10)))) != 0 &&
                i % ((int) ((clusteringTasks.size() / ((float) 10)))) == 0) {
            cout << currentDateTime() << " - Processed " <<
                    (int) (i / ((clusteringTasks.size() / (float) 10) + 1) + 1) * 10 << "%" << endl;
        } else if (i == clusteringTasks.size() - 1) {
            cout << currentDateTime() << " - Processed 100 %" << endl;
        }*/
        ClusteringTask *currTask = clusteringTasks[i];
#ifdef DEBUG
        cout << "Thread " << omp_get_thread_num() << " is clustering task " << *currTask << endl;
#endif
        currTask->cluster();
#pragma omp critical
        {
            //save memory, as we already don't need the pointers to this task's data any more
            coords_by_pix_it got = obsInCell->find(currTask->getTaskID());
            obsInCell->erase(got);
            got = obsInOverlap->find(currTask->getTaskID());
            obsInOverlap->erase(got);
        }
#ifdef DEBUG
        cout << "Task result: " << *currTask->getResult() << endl;
#endif

    }
}

void ClusteringController::freeMemory(coords_list *obsCoords,
        coords_by_pix *obsInCell,
        coords_by_pix *obsInOverlap) {
#ifdef DEBUG
    cout << "Freeing memory" << endl;
#endif
    delete csvOperator;
    delete chunkOperator;
    delete obsInCell;
    delete obsInOverlap;

    for (cluster_distinct_map_it it = result.begin(); it != result.end(); it++) {
        Coordinate * clusterID = it->first;
        set<Coordinate *>* clusterMembers = it->second;
        delete clusterID;
        delete clusterMembers;
    }
    delete obsCoords;
}




