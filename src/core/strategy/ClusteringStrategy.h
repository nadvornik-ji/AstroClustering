/* 
 * File:   ClusteringStrategy.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 12, 2015, 12:33 PM
 */

#ifndef CLUSTERINGSTRATEGY_H
#define	CLUSTERINGSTRATEGY_H
#include "../../helpers/ClusteringTypes.h"

/**
 * Abstract strategy used for clustering the data in a ClusteringTask
 * 
 * @param obsInCell Reference to the sources inside pixels 
 * @param obsInOverlap Reference to sources in pixel's overlaps
 */
class ClusteringStrategy {
public:
    ClusteringStrategy(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap);
    virtual ~ClusteringStrategy();
    
    /**
     * Abstract method used to cluster the data. Each strategy has this method
     * different.
     * 
     * @param catalog Resulting catalog
     */
    virtual void cluster(cluster_map * const catalog) = 0;
protected:
    coords_pointer_list *obsInCell;
    coords_pointer_list *obsInOverlap;
    
    /**
     * No matter what strategy we use, we have to get rid of clusters which were
     * sliced at the edge of a pixel. This method is common to all strategies.
     * 
     * @param catalog Resulting catalog
     */
    void removeIncompleteClusters(cluster_map * const catalog);

};

#endif	/* CLUSTERINGSTRATEGY_H */

