/* 
 * File:   EMStrategy.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 15, 2015, 10:28 PM
 */

#ifndef EMSTRATEGY_H
#define	EMSTRATEGY_H

#include "ClusteringStrategy.h"
#include "../../helpers/ClusteringTypes.h"

/**
 * Expectation maximization algorithm. It comes from Armadillo library.
 * 
 * This strategy is in debugging version yet.
 * 
 * !!!This algorithm had bad results and is not used ATM!!! Maybe we will figure
 * out how to improve the results later.
 * 
 * @param obsInCell
 * @param obsInOverlap
 */
class EMStrategy: public ClusteringStrategy {
public:
    EMStrategy(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap);
    virtual ~EMStrategy();
    
    void cluster(cluster_map * const catalog);
private:

};

#endif	/* EMSTRATEGY_H */

