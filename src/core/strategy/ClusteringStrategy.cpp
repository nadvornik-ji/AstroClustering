/* 
 * File:   ClusteringStrategy.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 12, 2015, 12:33 PM
 */

#include "ClusteringStrategy.h"
#include "../../helpers/ClusteringTypes.h"

ClusteringStrategy::ClusteringStrategy(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap) {
    this->obsInCell = obsInCell;
    this->obsInOverlap = obsInOverlap;
}

ClusteringStrategy::~ClusteringStrategy() {
}

void ClusteringStrategy::removeIncompleteClusters(cluster_map * const catalog) {
    for (cluster_map_it catalogIt = catalog->begin(); 
            catalogIt != catalog->end(); ) {
        Coordinate * clusterID = catalogIt->first;
        coords_pointer_list clusterMembers = catalogIt->second;
        
        bool allClusterMembersInOverlap = true;
        for (coords_list_it clusterIt = clusterMembers.begin(); 
                clusterIt != clusterMembers.end(); clusterIt++) {
            Coordinate * currClusterMember = *clusterIt;
            
            coords_list_it got = find((*obsInOverlap).begin(), (*obsInOverlap).end(), 
                    currClusterMember);
            
            if (got == (*obsInOverlap).end()) {
                allClusterMembersInOverlap = false;
                break;
            }
        }
        if (allClusterMembersInOverlap) {
#ifdef DEBUG_STRATEGY
            cout << "erasing incomplete cluster" <<  *clusterID << endl;
#endif
            delete clusterID;
            catalog->erase(catalogIt++);
        } else {
            catalogIt++;
        }
    }
}

