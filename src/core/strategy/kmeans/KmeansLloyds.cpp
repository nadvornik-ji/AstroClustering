/* 
 * File:   KmeansLloyds.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 19, 2015, 11:12 PM
 */

#include "KmeansLloyds.h"

KmeansLloyds::KmeansLloyds(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap) :
KmeansStrategy(obsInCell, obsInOverlap) {
}

KmeansLloyds::~KmeansLloyds() {
}

void KmeansLloyds::runHeurestic(KMfilterCenters &ctrs, KMdata &dataPts, KMterm &term) {
    (void) dataPts;
    KMlocalLloyds kmLloyds(ctrs, term); // repeated Lloyd's
    ctrs = kmLloyds.execute(); // execute 

#ifdef DEBUG_STRATEGY
    printSummary(kmLloyds, dataPts, ctrs); // print summary
#endif
}

