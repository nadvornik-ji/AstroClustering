/* 
 * File:   KmeansHybrid.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 19, 2015, 11:20 PM
 */

#include "KmeansHybrid.h"

KmeansHybrid::KmeansHybrid(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap) :
KmeansStrategy(obsInCell, obsInOverlap) {
}


KmeansHybrid::~KmeansHybrid() {
}

void KmeansHybrid::runHeurestic(KMfilterCenters &ctrs, KMdata &dataPts, KMterm &term) {
    (void) dataPts;
    KMlocalHybrid kmHybrid(ctrs, term); // Hybrid heuristic
    ctrs = kmHybrid.execute(); 

#ifdef DEBUG_STRATEGY
    printSummary(kmHybrid, dataPts, ctrs);
#endif
}