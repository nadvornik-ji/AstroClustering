/* 
 * File:   KmeansEZHybrid.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 19, 2015, 11:19 PM
 */

#include "KmeansEZHybrid.h"

KmeansEZHybrid::KmeansEZHybrid(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap) :
KmeansStrategy(obsInCell, obsInOverlap) {
}

KmeansEZHybrid::~KmeansEZHybrid() {
}

void KmeansEZHybrid::runHeurestic(KMfilterCenters &ctrs, KMdata &dataPts, KMterm &term) {
    (void) dataPts;
    KMlocalEZ_Hybrid kmEZ_Hybrid(ctrs, term); // EZ-Hybrid heuristic
    ctrs = kmEZ_Hybrid.execute();

#ifdef DEBUG_STRATEGY
    printSummary(kmEZ_Hybrid, dataPts, ctrs);
#endif
}