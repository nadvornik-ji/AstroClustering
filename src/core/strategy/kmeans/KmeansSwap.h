/* 
 * File:   KmeansSwap.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 19, 2015, 11:19 PM
 */

#ifndef KMEANSSWAP_H
#define	KMEANSSWAP_H

#include "KmeansStrategy.h"
#include "KMlocal.h"
#include "../../../helpers/ClusteringTypes.h"


class KmeansSwap : public KmeansStrategy{
public:
    KmeansSwap(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap);
    virtual ~KmeansSwap();
private:
    void runHeurestic(KMfilterCenters &ctrs, KMdata &dataPts, KMterm &term);
};

#endif	/* KMEANSSWAP_H */

