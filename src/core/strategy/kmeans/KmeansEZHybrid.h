/* 
 * File:   KmeansEZHybrid.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 19, 2015, 11:19 PM
 */

#ifndef KMEANSEZHYBRID_H
#define	KMEANSEZHYBRID_H

#include "KmeansStrategy.h"
#include "KMlocal.h"
#include "../../../helpers/ClusteringTypes.h"


class KmeansEZHybrid : public KmeansStrategy{
public:
    KmeansEZHybrid(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap);
    virtual ~KmeansEZHybrid();
private:
    void runHeurestic(KMfilterCenters &ctrs, KMdata &dataPts, KMterm &term);
};

#endif	/* KMEANSEZHYBRID_H */

