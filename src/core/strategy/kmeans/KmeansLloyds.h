/* 
 * File:   KmeansLloyds.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 19, 2015, 11:12 PM
 */

#ifndef KMEANSLLOYDS_H
#define	KMEANSLLOYDS_H

#include "KmeansStrategy.h"
#include "KMlocal.h"
#include "../../../helpers/ClusteringTypes.h"


class KmeansLloyds : public KmeansStrategy{
public:
    KmeansLloyds(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap);
    virtual ~KmeansLloyds();
private:
    void runHeurestic(KMfilterCenters &ctrs, KMdata &dataPts, KMterm &term);

};

#endif	/* KMEANSLLOYDS_H */

