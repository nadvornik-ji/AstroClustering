/* 
 * File:   KmeansHybrid.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 19, 2015, 11:20 PM
 */

#ifndef KMEANSHYBRID_H
#define	KMEANSHYBRID_H

#include "KmeansStrategy.h"
#include "KMlocal.h"
#include "../../../helpers/ClusteringTypes.h"

class KmeansHybrid : public KmeansStrategy{
public:
    KmeansHybrid(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap);
    virtual ~KmeansHybrid();
private:
    void runHeurestic(KMfilterCenters &ctrs, KMdata &dataPts, KMterm &term);

};

#endif	/* KMEANSHYBRID_H */

