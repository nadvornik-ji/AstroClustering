/* 
 * File:   KmeansSwap.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 19, 2015, 11:19 PM
 */

#include "KmeansSwap.h"

KmeansSwap::KmeansSwap(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap) :
KmeansStrategy(obsInCell, obsInOverlap) {
}

KmeansSwap::~KmeansSwap() {
}

void KmeansSwap::runHeurestic(KMfilterCenters &ctrs, KMdata &dataPts, KMterm &term) {
    (void) dataPts;
    KMlocalSwap kmSwap(ctrs, term); // Swap heuristic
    ctrs = kmSwap.execute();

#ifdef DEBUG_STRATEGY
    printSummary(kmSwap, dataPts, ctrs);
#endif
}


