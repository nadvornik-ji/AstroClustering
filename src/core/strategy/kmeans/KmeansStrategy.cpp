/* 
 * File:   KmeansStrategy.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 13, 2015, 1:36 PM
 */

#include "KmeansStrategy.h"
#include "../../../helpers/ClusteringTypes.h"
#include "../../../helpers/StreamOverloads.h"
#include "KMlocal.h"
#include "../../../Config.h"


using namespace std;

KmeansStrategy::KmeansStrategy(coords_pointer_list *obsInCell,
        coords_pointer_list *obsInOverlap) :
ClusteringStrategy::ClusteringStrategy(obsInCell, obsInOverlap) {
    max_k = Config::maxClusters;
}

KmeansStrategy::~KmeansStrategy() {
}

void KmeansStrategy::cluster(cluster_map * const catalog) {
    int dim = 3; // dimension
    int maxPts = obsInCell->size() + obsInOverlap->size(); // max number of data points
    int stages = 1000; // number of stages


    KMterm term(Config::maxTotStageVec0,
            Config::maxTotStageVec1,
            Config::maxTotStageVec2,
            Config::maxTotStageVec3, // run for 100 stages
            Config::minConsecRDL, // min consec RDL
            Config::minAccumRDL, // min accum RDL
            Config::maxRunStages, // max run stages
            Config::initProbAccept, // init. prob. of acceptance
            Config::tempRunLengt, // temp. run length
            Config::tempReducFact); // temp. reduction factor
    term.setAbsMaxTotStage(stages); // set number of stages

    //trivial example
    if (maxPts == 1) {
        Coordinate * centerCoord = new Coordinate((*obsInCell)[0]->ra, (*obsInCell)[0]->dec,
                -1, -1);
        (*catalog)[centerCoord].push_back((*obsInCell)[0]);
        return;
    }

    runKmeans(dim, maxPts, term, catalog);
    removeIncompleteClusters(catalog);
}

void KmeansStrategy::fillDataMatrix(KMdata &dataPts, int dim) {
    (void) dim;
    int nPts = 0; // actual number of points
    // generate points
    for (coords_list_it it = obsInCell->begin(); it != obsInCell->end(); it++) {
        dataPts[nPts][0] = (*it)->get3DVector().x;
        dataPts[nPts][1] = (*it)->get3DVector().y;
        dataPts[nPts][2] = (*it)->get3DVector().z;
        nPts++;
    }
    for (coords_list_it it = obsInOverlap->begin(); it != obsInOverlap->end(); it++) {
        dataPts[nPts][0] = (*it)->get3DVector().x;
        dataPts[nPts][1] = (*it)->get3DVector().y;
        dataPts[nPts][2] = (*it)->get3DVector().z;
        nPts++;
    }
#ifdef DEBUG_STRATEGY
    cout << "Data Points: " << dataPts.getNPts() << "\n"; // echo data points
#endif
}

bool KmeansStrategy::checkBetterResult(int k, int &bestK, KMfilterCenters &ctrs,
        KMfilterCenters **bestResult, KMfilterCenters **prevResult) {
    bool foundBetter = false;
    if (k == 1 || ((ctrs.getAvgDist(false)) <
            (*prevResult)->getAvgDist(false) / Config::elbowFact)) {
#ifdef DEBUG_STRATEGY
        cout << "we got better result for " << k << " !" << endl;
#endif
        delete (*bestResult);
        *bestResult = new KMfilterCenters(ctrs);
        bestK = k;
        foundBetter = true;
    }
#ifdef DEBUG_STRATEGY
    cout << "avgDistortion: " << ctrs.getAvgDist(false) << endl;
#endif 

    delete (*prevResult);
    *prevResult = new KMfilterCenters(ctrs);
    return foundBetter;
}

void KmeansStrategy::collectResults(KMdata &dataPts, KMfilterCenters *bestResult,
        int bestK, cluster_map * const catalog) {
    KMcenterArray centers = bestResult->getCtrPts();
    KMctrIdxArray closeCtr = new KMctrIdx[dataPts.getNPts()];
    double* sqDist = new double[dataPts.getNPts()];
    bestResult->getAssignments(closeCtr, sqDist);
#ifdef DEBUG_STRATEGY
    cout << "Best k is: " << bestK << endl;
#endif
    for (int i = 0; i < bestK; i++) {
        double x = centers[i][0];
        double y = centers[i][1];
        double z = centers[i][2];
        vec3 centerV(x, y, z);
        Coordinate * centerCoord = new Coordinate(centerV);
        for (int j = 0; j < dataPts.getNPts(); j++) {
            if (closeCtr[j] == i) {
                if (j < (int) obsInCell->size()) {
                    (*catalog)[centerCoord].push_back((*obsInCell)[j]);
                } else {
                    (*catalog)[centerCoord].push_back((*obsInOverlap)[j - obsInCell->size()]);
                }
            }
        }
    }
    delete [] closeCtr;
    delete [] sqDist;
}

void KmeansStrategy::runKmeans(int dim, int maxPts, KMterm &term,
        cluster_map * const catalog) {
    KMfilterCenters *bestResult = NULL, *prevResult = NULL;
    int bestK = 0;

#ifdef DEBUG_STRATEGY
    cout << "Transfering data" << endl;
    cout << "ObsInCell size " << obsInCell->size() << ": " << endl << *obsInCell << endl;
    cout << "ObsInOverlap size " << obsInOverlap->size() << ": " << endl << *obsInOverlap << endl;
#endif

    KMdata dataPts(dim, maxPts); // allocate data storage
    fillDataMatrix(dataPts, dim);

    dataPts.setNPts(maxPts); // set actual number of pts
    dataPts.buildKcTree(); // build filtering structure

    for (int k = 1; k < max_k && k < maxPts; k++) {
        KMfilterCenters ctrs(k, dataPts); // allocate centers
#ifdef DEBUG_STRATEGY
        cout << "\nExecuting Clustering Algorithm for k " << k << " :\n";
#endif
        runHeurestic(ctrs, dataPts, term);
        if (!checkBetterResult(k, bestK, ctrs, &bestResult, &prevResult)) {
            break;  //in the end we don't want to check all of the numbers.
        };
    }

    collectResults(dataPts, bestResult, bestK, catalog);
    delete prevResult;
    delete bestResult;
}


//----------------------------------------------------------------------
//  Reading/Printing utilities
//	readPt - read a point from input stream into data storage
//		at position i.  Returns false on error or EOF.
//	printPt - prints a points to output file
//----------------------------------------------------------------------

bool KmeansStrategy::readPt(istream& in, KMpoint& p, int dim) {
    for (int d = 0; d < dim; d++) {
        if (!(in >> p[d])) return false;
    }
    return true;
}

void KmeansStrategy::printPt(ostream& out, const KMpoint& p, int dim) {
    out << "(" << p[0];
    for (int i = 1; i < dim; i++) {
        out << ", " << p[i];
    }
    out << ")\n";
}

//------------------------------------------------------------------------
//  Print summary of execution
//------------------------------------------------------------------------

void KmeansStrategy::printSummary(
        const KMlocal& theAlg, // the algorithm
        const KMdata& dataPts, // the points
        KMfilterCenters & ctrs) // the centers
{
    cout << "Number of stages: " << theAlg.getTotalStages() << "\n";
    cout << "Average distortion: " <<
            ctrs.getDist(false) / double(ctrs.getNPts()) << "\n";
    // print final center points
    cout << "(Final Center Points:\n";
    ctrs.print();
    cout << ")\n";
    // get/print final cluster assignments
    KMctrIdxArray closeCtr = new KMctrIdx[dataPts.getNPts()];
    double* sqDist = new double[dataPts.getNPts()];
    ctrs.getAssignments(closeCtr, sqDist);

    *kmOut << "(Cluster assignments:\n"
            << "    Point  Center  Squared Dist\n"
            << "    -----  ------  ------------\n";
    for (int i = 0; i < dataPts.getNPts(); i++) {
        *kmOut << "   " << setw(5) << i
                << "   " << setw(5) << closeCtr[i]
                << "   " << setw(10) << sqDist[i]
                << "\n";
    }
    *kmOut << ")\n";
    delete [] closeCtr;
    delete [] sqDist;
}


