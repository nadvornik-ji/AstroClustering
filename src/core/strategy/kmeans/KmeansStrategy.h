/* 
 * File:   KmeansStrategy.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 13, 2015, 1:36 PM
 */
#include "KMlocal.h"

#ifndef KMEANSSTRATEGY_H
#define	KMEANSSTRATEGY_H

#include "../ClusteringStrategy.h"
#include "../../../helpers/ClusteringTypes.h"

/**
 * Abstract class common to all of the variants of K-means algorithm. All of
 * these versions come from KMLocal library. All of them provide reasonable 
 * results, the problem comes only with using the correct "elbow method" - 
 * deciding whether we should increase the K or the mean square error won't
 * improve enough if we did so - and the number of clusters is optimal.
 * 
 * @param obsInCell
 * @param obsInOverlap
 */
class KmeansStrategy : public ClusteringStrategy {
public:
    KmeansStrategy(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap);
    virtual ~KmeansStrategy();

    void cluster(cluster_map * const catalog);
protected:
    /**
     * Fill the KMData matrix used by KMLocal algorithm
     * 
     * @param dataPts Output parameter 
     * @param dim Number of dimensions
     */
    void fillDataMatrix(KMdata &dataPts, int dim);
    
    /**
     * The elbow method. Checks whether we have the best number of clusters or
     * should try with more clusters.
     * 
     * @param k Number of clusters
     * @param bestK Best number of clusters so far according to the mean square
     * error
     * @param ctrs KMLocal centers
     * @param bestResult Reference to the best result so far
     * @param prevResult Reference to the previous result
     * @return Should increase number of clusters?
     */
    bool checkBetterResult(int k, int &bestK, KMfilterCenters &ctrs,
            KMfilterCenters **bestResult, KMfilterCenters **prevResult);
    
    /**
     * Collects the results from the KMLocal data structures into our resulting
     * catalog.
     * 
     * @param dataPts KMData points
     * @param bestResult Best clustering result
     * @param bestK Best number of clusters
     * @param catalog Resulting catalog
     */
    void collectResults(KMdata &dataPts, KMfilterCenters *bestResult,
            int bestK, cluster_map * const catalog);
    
    /**
     * Run the algorithm, this is a controller for this strategy. It prepares 
     * the KMLocal data structures and runs the KMeans iterations in the 
     * runHeurestic method.
     * 
     * @param dim
     * @param maxPts
     * @param term
     * @param catalog
     */
    void runKmeans(int dim, int maxPts, KMterm &term, 
            cluster_map * const catalog);

    /**
     * The actual body of the clustering strategy - this is different for each
     * version of KMeans algorithm used here - can be Lloyds, EZHybrid, Hybrid
     *  or Swap.
     * 
     * @param ctrs The cluster centers
     * @param dataPts Source detections
     * @param term Termination conditions
     */
    virtual void runHeurestic(KMfilterCenters &ctrs, KMdata &dataPts, KMterm &term) = 0;

    void printSummary(// print final summary
            const KMlocal& theAlg, // the algorithm
            const KMdata& dataPts, // the points
            KMfilterCenters& ctrs); // the centers
    bool readPt(// read a point
            istream& in, // input stream
            KMpoint& p,
            int dim); // point (returned)
    void printPt(// print a point
            ostream& out, // output stream
            const KMpoint& p,
            int dim); // the point

    int max_k;
};

#endif	/* KMEANSSTRATEGY_H */

