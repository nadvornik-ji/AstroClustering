/* 
 * File:   IncrementalClusteringStrategy.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 12, 2015, 12:33 PM
 */

#ifndef INCREMENTALCLUSTERINGSTRATEGY_H
#define	INCREMENTALCLUSTERINGSTRATEGY_H

#include "ClusteringStrategy.h"
#include "../../helpers/ClusteringTypes.h"
#include <map>


/**
 * Easiest strategy we use. It is a hybrid version of incremental K-means 
 * algorithm which takes the sources one by one and tries to match them to 
 * existing centroids by a fixed angular distance (Config::clusterRadius). If 
 * it cannot match an existing centroid, it creates a new one. 
 * 
 * With this method we get very fast and accurate results for clusters which are
 * not closer to each other than the Config::clusterRadius.
 * 
 * @param obsInCell Reference to sources inside pixels
 * @param obsInOverlap Reference to sources in pixel's overlaps
 */
class IncrementalStrategy : public ClusteringStrategy {
public:
    IncrementalStrategy(coords_pointer_list *obsInCell, 
            coords_pointer_list *obsInOverlap);
    virtual ~IncrementalStrategy();
    
    void cluster(cluster_map * const taskResult);
private:
    /**
     * Tries to find a neighboring cluster closer than Config::clusterRadius and 
     * update it. 
     * 
     * @param indexID Index to search by in the current catalog
     * @param currObs Current source
     * @param catalog Resulting catalog of this ClusteringTask
     * @return Is there a cluster within Config::clusterRadius?
     */
    bool findAndUpdateNeighbor(int64 indexID, Coordinate * const currObs, 
            cluster_map * const catalog);
    
    /**
     * Abstract method to process both observations inside the pixel and the 
     * observations in overlapping region.
     * 
     * @param obsList Either obsInCell or obsInOverlap
     * @param catalog Resulting catalog
     */
    void processObservations(const coords_pointer_list & obsList, 
            cluster_map * const catalog);
    /**
     * Returns the clusters within Config::clusterRadius in the closeClusters
     * output variable.
     * @param indexID HEALPix index of the cell
     * @return 
     */
    coords_pointer_list getClosePoints(int64 indexID);
    /**
     * Updates neighboring cluster with the source currently being processed.
     * 
     * @param currNeighbor The neighbor to be updated
     * @param currObs Source to be added
     * @param catalog Resulting catalog
     */
    void updateNeighboringCluster(Coordinate * const currNeighbor, 
        Coordinate * const currObs, cluster_map * const catalog);
            
    map<int64, vector<Coordinate *>> catalogIndex;
    Healpix_Base2 indexBase;
    

};

#endif	/* INCREMENTALCLUSTERINGSTRATEGY_H */

