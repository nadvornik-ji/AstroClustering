/* 
 * File:   IncrementalClusteringStrategy.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 12, 2015, 12:33 PM
 */

#include "IncrementalStrategy.h"
#include "../../helpers/ClusteringTypes.h"
#include "../../Config.h"
#include "healpix_base.h"
#include "../../helpers/HealPixHelper.h"
#include "../../helpers/StreamOverloads.h"

IncrementalStrategy::IncrementalStrategy(coords_pointer_list *obsInCell,
        coords_pointer_list *obsInOverlap) :
ClusteringStrategy::ClusteringStrategy(obsInCell, obsInOverlap) {
    indexBase = Healpix_Base2(Config::catalogIndexNside, NEST, SET_NSIDE);
}

IncrementalStrategy::~IncrementalStrategy() {
}

void IncrementalStrategy::cluster(cluster_map * const taskResult) {

    processObservations(*obsInCell, taskResult);
    processObservations(*obsInOverlap, taskResult);
    removeIncompleteClusters(taskResult);
}

void IncrementalStrategy::processObservations(
        const coords_pointer_list & obsList,
        cluster_map * const taskResult) {
    for (coords_list_const_it it = obsList.begin(); it != obsList.end(); it++) {
        Coordinate * currObs = *it;
        int64 indexID = indexBase.ang2pix(currObs->getHealPixPointing());

        bool catalogUpdated = false;
        catalogUpdated = findAndUpdateNeighbor(indexID, currObs, taskResult);

        if (!catalogUpdated) {
            Coordinate * clusterID = new Coordinate(currObs->ra, currObs->dec, -1, -1);
#ifdef DEBUG_STRATEGY
            cout << "Inserting cluster" << *clusterID << endl;
#endif
            (*taskResult)[clusterID].push_back(currObs);
            catalogIndex[indexID].push_back(clusterID);
#ifdef DEBUG_STRATEGY
            cout << "Catalog after insertion: " << taskResult << endl;
#endif
        }
    }
}

bool IncrementalStrategy::findAndUpdateNeighbor(int64 indexID,
        Coordinate * const currObs, cluster_map * const taskResult) {
    coords_pointer_list clustersCloseToMe = getClosePoints(indexID);

    for (coords_list_it it = clustersCloseToMe.begin(); it != clustersCloseToMe.end();
            it++) {
        Coordinate * currNeighborClusterID = *it;

        double distance = HealPixHelper::computeDistance(*currObs, *currNeighborClusterID);

        if (distance < Config::clusterRadius) {
            updateNeighboringCluster(currNeighborClusterID, currObs, taskResult);
            return true;
        }
    }
    return false;
}

void IncrementalStrategy::updateNeighboringCluster(Coordinate * const clusterID,
        Coordinate * const currObs, cluster_map * const taskResult) {
    size_t noClusterMembers = 0;

    cluster_map_it got = taskResult->find(clusterID);

    if (got != taskResult->end()) {
        noClusterMembers = got->second.size();
    }

    clusterID->ra = (clusterID->ra * noClusterMembers + currObs->ra) /
            (noClusterMembers + 1);
    clusterID->dec = (clusterID->dec * noClusterMembers + currObs->dec) /
            (noClusterMembers + 1);

    (*taskResult)[clusterID].push_back(currObs);
}

coords_pointer_list IncrementalStrategy::getClosePoints(
        int64 indexID) {
    coords_pointer_list neighboringCoordinates;
    fix_arr<int64, 8> myNeighborPixels;
    indexBase.neighbors(indexID, myNeighborPixels);

    coords_pointer_list closeClusters = coords_pointer_list(catalogIndex[indexID]);

    for (size_t i = 0; i < myNeighborPixels.size(); i++) {
        coords_pointer_list myNeighboringPixelPoints = catalogIndex[myNeighborPixels[i]];

        for (coords_list_it it = myNeighboringPixelPoints.begin();
                it != myNeighboringPixelPoints.end(); it++) {
            closeClusters.push_back(*it);
        }
    }
    return closeClusters;
}


