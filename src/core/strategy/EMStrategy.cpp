/* 
 * File:   EMStrategy.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 15, 2015, 10:29 PM
 */

#include "EMStrategy.h"
#include "armadillo"
#include "../../Config.h"

using namespace std;

EMStrategy::EMStrategy(coords_pointer_list *obsInCell, coords_pointer_list *obsInOverlap) :
ClusteringStrategy::ClusteringStrategy(obsInCell, obsInOverlap) {
}

EMStrategy::~EMStrategy() {
}

void EMStrategy::cluster(cluster_map * const catalog) {
    // create synthetic data with 2 Gaussians

    arma::uword N = obsInCell->size() + obsInOverlap->size();
    arma::uword dim = 3;
    int bestK = 5;

    arma::mat data(dim, N, arma::fill::none);

    //arma::vec mean0 = arma::linspace<arma::vec>(1, dim, dim);
    //arma::vec mean1 = mean0 + 2;

    for (size_t i = 0; i < N; i++) {
        if (i < obsInCell->size()) {
            vec3 healPixVector = (*obsInCell)[i]->get3DVector();
            arma::vec armaVector = arma::vec(dim);
            armaVector[0] = healPixVector.x;
            armaVector[1] = healPixVector.y;
            armaVector[2] = healPixVector.z;
            data.col(i) = armaVector;
        } else {
            vec3 healPixVector = (*obsInOverlap)[i - obsInCell->size()]->get3DVector();
            arma::vec armaVector = arma::vec(dim);
            armaVector[0] = healPixVector.x;
            armaVector[1] = healPixVector.y;
            armaVector[2] = healPixVector.z;
            data.col(i) = armaVector;
        }
    }
    cout << "data: " << data << endl;


    arma::gmm_diag model;

    cout << "learning " << endl;
    cout << "varFloor: " << Config::varFloor << endl;
    model.learn(data, bestK, arma::eucl_dist, arma::static_subset, 0, 100, Config::varFloor, true);
    model.means.print("means:");

    //double scalar_likelihood = model.log_p(data.col(0));
    //arma::rowvec set_likelihood = model.log_p(data.cols(0, 9));

    //double overall_likelihood = model.avg_log_p(data);

    for (int i = 0; i < bestK; i++) {
        arma::vec gausVec = model.means.col(i);
        vec3 centerV(gausVec[0], gausVec[1], gausVec[2]);
        Coordinate * centerCoord = new Coordinate(centerV);
        cout << "centers: " << endl;
        cout << *centerCoord << endl;
    }
    

    for (int i = 0; i < bestK; i++) {
        arma::vec gausVec = model.means.col(i);
        vec3 centerV(gausVec[0], gausVec[1], gausVec[2]);
        Coordinate * centerCoord = new Coordinate(centerV);
        for (size_t j = 0; j < N; j++) {
            arma::uword gausIdx = model.assign(data.col(i), arma::eucl_dist);
            cout <<  "point " << j << " assigned to: " << gausIdx << endl;
            if ((int) gausIdx == i) {
                if ((int) j < (int) obsInCell->size()) {
                    (*catalog)[centerCoord].push_back((*obsInCell)[j]);
                } else {
                    (*catalog)[centerCoord].push_back((*obsInOverlap)[j - obsInCell->size()]);
                }
            }
        }
    }
    
    //arma::uword gaus_id = model.assign(data.col(0), arma::eucl_dist);
    //arma::urowvec gaus_ids = model.assign(data, arma::prob_dist);

    //arma::urowvec hist1 = model.raw_hist(data, arma::prob_dist);
    //arma::rowvec hist2 = model.norm_hist(data, arma::eucl_dist);

    //model.save("my_model.gmm");
}

