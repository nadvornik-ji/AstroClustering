/* 
 * File:   ClusteringController.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 12, 2015, 5:36 PM
 */

#ifndef CLUSTERINGCONTROLLER_H
#define	CLUSTERINGCONTROLLER_H
#include "../helpers/ClusteringTypes.h"
#include "ClusteringTask.h"
#include "ChunkOperator.h"
#include "../io/CsvOperator.h"
#include <boost/chrono/process_cpu_clocks.hpp>
#include <time.h>
#include <iomanip>
#include <fstream>
#include <omp.h>

using namespace std;
/**
 * Class responsible for controlling the methods used in the clustering process.
 * Has several methods which represent running stages of the program and these
 * are run in the run function of this class.
 * 
 * @param strategy Strategy of the clustering algorithm
 * @param noThreads Number of threads
 * @param inputPath CSV input path
 * @param catalogPath CSV output catalog path
 * @param relationsPath CSV output relations path
 */
class ClusteringController {
public:
    ClusteringController(string strategy, int noThreads, string inputPath,
            string catalogPath, string relationsPath);
    virtual ~ClusteringController();

    /**
     * This method contains the logic of the clustering algorithm. It reads the
     * coordinates from an input file, partitions the sources by HEALPix pixels
     * and creates the ClusteringTask instances which are clustered in parallel
     * by noThreads. Then it collects the results, removes duplicates and writes
     * the results into a catalog and relations output files.
     * @return Success/Failure
     */
    int run();

private:
    
    /**
     * Creates the clustering tasks from Coordinates lists grouped by HEALPix 
     * pixels. Fills the clusteringTasks property of this class.
     * @param obsInCell Observations in HEALPix cells grouped by the HEALPix
     * ID's of these cells.
     * @param obsInOverlap Observations in the overlapping regions of the 
     * HEALPix cells. Again identified by the HEALPix pixel ID.
     */
    void createTasks(coords_by_pix  * const obsInCell, 
            coords_by_pix * const obsInOverlap);

    /**
     * Clusters the tasks in parallel. When it processes the task, it finds it
     * in the obsInCell and obsInOverlap containers and deletes it to save 
     * memory.
     * @param obsInCell Container for the Coordinate structures
     * @param obsInOverlap Container for the Coordinate structures
     */
    void clusterTasks(coords_by_pix * const obsInCell, 
            coords_by_pix * const obsInOverlap);

    /**
     * Collects the results of all individual tasks and merges them. It removes
     * duplicate entries on edges of the pixels. This has to be run by one 
     * thread.
     */
    void collectResults();

    /**
     * Removes the collections in parameters from memory.
     * @param coords
     * @param obsInCell
     * @param obsInOverlap
     */
    void freeMemory(coords_list * coords,
            coords_by_pix * obsInCell,
            coords_by_pix * obsInOverlap);

    /**
     * Used by the collectResults method. Checks whether the resulting clusters
     * of this tasks aren't suspiciously close to some other already collected
     * clusters. If yes, it merges the resulting cluster to those clusters, as
     * it is apparently a duplicate result on the edge of the underlying 
     * HEALPix cells.
     * @param taskIndex HEALPix index of the underlying cell
     * @param result The result where we are storing the merged resulting 
     * clusters of our algorithm
     * @param currTaskID HEALPix index of the current task.
     * @param currClusterID Identifier for the current cluster, it is the 
     * a Coordinate structure representing the centroid of this cluster.
     * @param currClusterMembers Sources which were assigned to the current 
     * cluster
     * @return Whether the cluster was duplicate or not
     */
    bool checkAndUpdateDuplicateCluster(
            const task_index & taskIndex,
            cluster_distinct_map * const result,
            int64 currTaskID,
            const Coordinate & currClusterID,
            const coords_pointer_list & currClusterMembers);

    /**
     * Gets neighboring clusters of my current cluster
     * @param currTaskID HEALPix index of the underlying cell
     * @param taskIndex Index helping to spatially search in the tasks.
     * @return The IDs of the neighboring HEALPix 
     * cells
     */
    coords_pointer_list getNeighboringClusters(int64 currTaskID,
            const task_index & taskIndex);

    /**
     * Takes one cluster and merges it into the second. It takes all of the
     * points from the first cluster and checks and if they are not already
     * in the second, it inserts them there. The first cluster is discarded
     * afterwards. 
     * 
     * Because we are working only with coordinate pointers, we can be sure
     * that we are still working with the original sources we loaded from the
     * input CSV.
     * 
     * @param firstClusterID HEALPix ID of the first cluster
     * @param firstClusterMembers List of Coordinate pointers to the members of
     * first cluster
     * @param secondClusterID HEALPix ID of the secpnd cluster
     * @param secondClusterMembers List of Coordinate pointers to the members of
     * second cluster
     */
    void mergeFirstClusterIntoSecond(const Coordinate & firstClusterID,
            const coords_pointer_list & firstClusterMembers,
            Coordinate * const secondClusterID,
            set<Coordinate *> * const secondClusterMembers);
     
    /**
     * Get current date/time, format is YYYY-MM-DD.HH:mm:ss
     * @return 
     */
    const string currentDateTime() {
        time_t now = time(0);
        struct tm tstruct;
        char buf[80];
        tstruct = *localtime(&now);
        strftime(buf, sizeof (buf), "%Y-%m-%dT%X", &tstruct);
        return buf;
    }

    /**
     * Logs information about running times of the algorithm into a file.
     * 
     * @param file
     * @param userTime
     * @param systemTime
     * @param realTime
     */
    void logTimes(ostream &file, boost::chrono::duration<long int, boost::ratio<1l, 1000000000l> > userTime,
            boost::chrono::duration<long int, boost::ratio<1l, 1000000000l> > systemTime,
            boost::chrono::duration<long int, boost::ratio<1l, 1000000000l> > realTime) {
        cout << setw(16) << "user" << setw(16) << "system" << setw(16) << "real" << endl;
        cout << setw(16) << boost::chrono::duration_cast<boost::chrono::milliseconds>(userTime).count() <<
                setw(16) << boost::chrono::duration_cast<boost::chrono::milliseconds>(systemTime).count() <<
                setw(16) << boost::chrono::duration_cast<boost::chrono::milliseconds>(realTime).count() << endl;
        file << boost::chrono::duration_cast<boost::chrono::milliseconds>(userTime).count() << "," <<
                boost::chrono::duration_cast<boost::chrono::milliseconds>(systemTime).count() << "," <<
                boost::chrono::duration_cast<boost::chrono::milliseconds>(realTime).count() << endl;
    }

    string strategyStr, csvPath, catalogPath, relationsPath;
    int noThreads;

    //owned objects
    CsvOperator * csvOperator;
    ChunkOperator * chunkOperator;
    vector<ClusteringTask*> clusteringTasks;
    cluster_distinct_map result;


};

#endif	/* CLUSTERINGCONTROLLER_H */

