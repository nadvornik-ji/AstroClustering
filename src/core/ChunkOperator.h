/* 
 * File:   ChunkOperator.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 10, 2015, 12:33 PM
 */

#ifndef CHUNK_OPERATOR_H
#define CHUNK_OPERATOR_H
#include <memory>

#include "healpix_base.h"
#include <map>
#include "../model/Coordinate.h"
#include "../helpers/ClusteringTypes.h"

using namespace std;

/**
 * Class responsible for partitioning the input data into chunks representing
 * some distinct areas on the skies. It uses HEALPix pixels to do so and the
 * result is stored in its obsInCell and obsInOverlap property. 
 * 
 * The obsInCell holds HELPix pixel (size of Config::bigPixelNside) IDs pointing 
 * to all of the sources inside that pixel. 
 * 
 * obsInOverlap holds all of the sources in the pixel's overlapping region. The
 * size of this region is defined by the Config::overlapPixelNside property.
 * 
 */
class ChunkOperator {

public:

    ChunkOperator(coords_list *observations_degrees) ;
    ~ChunkOperator();

    coords_by_pix *getObservationsInOverlap() const;
    coords_by_pix *getObservationsInCell() const;
    coords_list *getObservationCoordinates() const;

    int main();
    /**
     * Takes the loaded coordinates of sources and partitions them into chunks.
     * 
     * @param base1Nside Size of the pixels in the HEALPix grid
     * @param base2Nside Size of the overlaps in the HEALPix grid
     * @return Success/failure
     */
    int buildChunksFromCoordinates(int64 base1Nside, int64 base2Nside);

private:
    coords_list *obsCoords;
    coords_by_pix *obsInCell, *obsInOverlap;
};


#endif