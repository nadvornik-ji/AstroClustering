/* 
 * File:   Config.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 8, 2015, 6:24 PM
 */

#include "Config.h"


//[parallelOptions]
int64 Config::bigPixelNside; //Resolution for the parallelization tasks
int64 Config::overlapPixelNside; //Resolution of the overlap of pixels

//[resultOptions]
int64 Config::IDNside; //Resolution used for generating catalog IDs
double Config::clusterDuplicatesJoinRad; //Distance at which two clusters will be identified as duplicates <arcsec>

//[incrementalStrategy]
int64 Config::catalogIndexNside; //Resolution of the index used for catalog inside one task
double Config::clusterRadius; //Distance at which I will add a point to the cluster <arcsec>

//Parameters for the kmlocal algorithm TODO link
//[kmeansLocalStrategy]	
int Config::maxClusters;
int Config::maxTotStageVec0;
int Config::maxTotStageVec1;
int Config::maxTotStageVec2;
int Config::maxTotStageVec3; //run for 100 stages
double Config::minConsecRDL; //min consec RDL
double Config::minAccumRDL; //min accum RDL
int Config::maxRunStages; //max run Stages
double Config::initProbAccept; //init probability of acceptance
int Config::tempRunLengt; //temp. run length
double Config::tempReducFact; //temp. reduction factor
double Config::elbowFact; //elbow Method aceptane factor

//[emStrategy]
double Config::varFloor;
//[paths]
std::string Config::timeLogPath;