/* 
 * File:   Config.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 8, 2015, 6:24 PM
 */

#ifndef CONFIG_H
#define	CONFIG_H
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

//#define DEBUG
//#define DEBUG_STRATEGY
//#define DEBUG_COLLECT
#define LOG_TIME
//#define DEBUG_VERBOSE
//#define STDOUT_VERBOSE

#define ARCSEC_TO_RAD 4.84813681e-6

#include "healpix_base.h"

using namespace std;

/**
 * Struct used for all loaded configurations.
 */
struct Config {
    //[parallelOptions]
    static int64 bigPixelNside;         //Resolution for the parallelization tasks
    static int64 overlapPixelNside;          //Resolution of the overlap of pixels

    //[resultOptions]
    static int64 IDNside;               //Resolution used for generating catalog IDs
    static double clusterDuplicatesJoinRad;    //Distance at which two clusters will be identified as duplicates <arcsec>

    //[incrementalStrategy]
    static int64 catalogIndexNside;     //Resolution of the index used for catalog inside one task
    static double clusterRadius;        //Distance at which I will add a point to the cluster <arcsec>

    //Parameters for the kmlocal algorithm TODO link
    //[kmeansLocalStrategy]	
    static int maxClusters;
    static int maxTotStageVec0;
    static int maxTotStageVec1;
    static int maxTotStageVec2;
    static int maxTotStageVec3;         //run for 100 stages
    static double minConsecRDL;         //min consec RDL
    static double minAccumRDL;          //min accum RDL
    static int maxRunStages;             //max run Stages
    static double initProbAccept;       //init probability of acceptance
    static int tempRunLengt;            //temp. run length
    static double tempReducFact;       //temp. reduction factor
    static double elbowFact;           //elbow Method aceptane factor
    
    //[emStrategy]
    static double varFloor;
    //[paths]
    static string timeLogPath;

    /**
     * Parses the config.ini file with a boost read_ini function and initializes
     * global Config structure used anywhere in our program.
     */
    static void initialize() {
        boost::property_tree::ptree pt;
        try {
            boost::property_tree::ini_parser::read_ini("config.ini", pt);
        } catch (const exception& e) {
            cerr << "Could not parse config.ini" << endl;
            cerr << e.what() << endl;
            exit(1);
        }
        int bigPixelNsideExp = stoi(pt.get<string>("parallelOptions.bigPixelNsideExp"));
        bigPixelNside = 1 << bigPixelNsideExp;
        int overlapPixelNsideExp = stoi(pt.get<string>("parallelOptions.overlapPixelNsideExp"));
        overlapPixelNside = 1 << overlapPixelNsideExp;
        int IDNsideExp = stoi(pt.get<string>("resultOptions.IDNsideExp"));
        IDNside = 1 << IDNsideExp;
        double clusterDuplicatesArcSec = stod(pt.get<string>("resultOptions.clusterDuplicatesArcSec"));
        clusterDuplicatesJoinRad = clusterDuplicatesArcSec * ARCSEC_TO_RAD;
        int catalogIndexNsideExp = stoi(pt.get<string>("incrementalStrategy.catalogIndexNsideExp"));
        catalogIndexNside = 1 << catalogIndexNsideExp;
        double clusterRadiusArcSec = stod(pt.get<string>("incrementalStrategy.clusterRadiusArcSec"));
        clusterRadius = clusterRadiusArcSec * ARCSEC_TO_RAD;
        maxClusters = stoi(pt.get<string>("kmeansLocalStrategy.maxClusters"));
        maxTotStageVec0 = stoi(pt.get<string>("kmeansLocalStrategy.maxTotStageVec0"));
        maxTotStageVec1 = stoi(pt.get<string>("kmeansLocalStrategy.maxTotStageVec1"));
        maxTotStageVec2 = stoi(pt.get<string>("kmeansLocalStrategy.maxTotStageVec2"));
        maxTotStageVec3 = stoi(pt.get<string>("kmeansLocalStrategy.maxTotStageVec3"));
        minConsecRDL = stod(pt.get<string>("kmeansLocalStrategy.minConsecRDL"));
        minAccumRDL = stod(pt.get<string>("kmeansLocalStrategy.minAccumRDL"));
        maxRunStages = stoi(pt.get<string>("kmeansLocalStrategy.maxRunStage"));
        initProbAccept = stod(pt.get<string>("kmeansLocalStrategy.initProbAccept"));
        tempRunLengt = stoi(pt.get<string>("kmeansLocalStrategy.tempRunLengt"));
        tempReducFact = stod(pt.get<string>("kmeansLocalStrategy.tempReducFact"));
        elbowFact = stod(pt.get<string>("kmeansLocalStrategy.elbowFact"));
        varFloor = stod(pt.get<string>("emStrategy.varFloor"));
        timeLogPath = pt.get<string>("paths.timeLogPath");
    }
};

#endif	/* CONFIG_H */

