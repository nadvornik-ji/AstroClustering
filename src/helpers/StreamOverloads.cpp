/* 
 * File:   StreamOverloads.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 9, 2015, 6:11 AM
 */

#include "StreamOverloads.h"
#include "ClusteringTypes.h"
#include "../Config.h"
#include "healpix_base.h"

using namespace std;

ostream& operator<<(ostream &strm, const ClusteringTask &t) {
    Healpix_Base2 base(15, NEST);
    pointing pt = base.pix2ang(t.getTaskID());
    Coordinate coord = Coordinate(pt.to_vec3());
    strm << "ClusteringTask:" << '\n';
    strm << "  taskID: " << t.getTaskID() << '\n';
    strm << "  " << coord << endl;
    strm << "  cellObservations: " << t.getObsInCell()->size() << '\n';
    strm << "  overlapObservations: " << t.getObsInOverlap()->size() << '\n';
    return strm;
}

ostream& operator<<(ostream &strm, const cluster_map &res) {
    strm << "Task Result of size: " << res.size() << endl;
    for (cluster_map_const_it it = res.begin(); it != res.end(); ++it) {
        strm << "  ClusterID: " << *it->first << " with " << it->second.size()
                << " members" << endl;
#ifdef DEBUG_VERBOSE
        for (size_t i = 0; i < it->second.size(); i++) {
            strm << "    " << *it->second[i] << endl;
        }
#endif
    }
    return strm;
}

ostream& operator<<(ostream &strm, const cluster_distinct_map &res) {
    strm << "  Task Result of size: " << res.size() << endl;
    for (cluster_distinct_map_const_it it = res.begin(); it != res.end(); ++it) {
        strm << "    ClusterID: " << *it->first << " with " << it->second->size()
                << " members" << endl;
#ifdef DEBUG_VERBOSE
        set<Coordinate *>::iterator set_it;
        for (set_it = it->second->begin(); set_it != it->second->end(); set_it++) {
            strm << "      " << **set_it << endl;
        }
#endif
    }
    return strm;
}

ostream& operator<<(ostream &strm, const map<int64, coords_pointer_list*> &taskIndex) {
    strm << "ClusterIndex: " << endl;

    for (task_index_const_it it = taskIndex.begin(); it != taskIndex.end(); it++) {
        strm << "TaskID: " << it->first << endl;
        strm << *it->second;
    }
    return strm;
}

ostream& operator<<(ostream &strm, const coords_pointer_list &list) {
    strm << "List of " << list.size() << " coordinates: " << endl;
#ifdef DEBUG_VERBOSE
    for (coords_list_const_it it = list.begin(); it != list.end(); it++) {
        strm << **it << endl;
    }
#endif
    return strm;
}
