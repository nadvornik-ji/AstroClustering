/* 
 * File:   StreamOverloads.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 9, 2015, 6:15 AM
 */

#ifndef STREAMOVERLOADS_H
#define	STREAMOVERLOADS_H
#include <iostream>
#include <memory>
#include "../core/ClusteringTask.h"
#include "../model/Coordinate.h"
#include "ClusteringTypes.h"

ostream& operator<<(ostream &strm, const ClusteringTask &t);
ostream& operator<<(ostream &strm, const cluster_map &res);
ostream& operator<<(ostream &strm, const cluster_distinct_map &res);
ostream& operator<<(ostream &strm, const map<int64, coords_pointer_list*> &taskIndex);
ostream& operator<<(ostream &strm, const coords_pointer_list &list);

#endif	/* STREAMOVERLOADS_H */

