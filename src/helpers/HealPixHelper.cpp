/* 
 * File:   HealPixHelper.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 * 
 * Created on April 8, 2015, 6:42 PM
 */

#include "HealPixHelper.h"
#include "../model/Coordinate.h"

using namespace std;


double HealPixHelper::computeDistance(const Coordinate & currObs, 
        const Coordinate & currNeighbor) {

    pointing currMyPointing = currObs.getHealPixPointing();
    pointing currNeighborPointing = currNeighbor.getHealPixPointing();

    double st1 = sin(currMyPointing.theta);
    double x1 = st1 * cos(currMyPointing.phi);
    double y1 = st1 * sin(currMyPointing.phi);
    double z1 = cos(currMyPointing.theta);
    vec3 v1 = vec3(x1, y1, z1);
    double st2 = sin(currNeighborPointing.theta);
    double x2 = st2 * cos(currNeighborPointing.phi);
    double y2 = st2 * sin(currNeighborPointing.phi);
    double z2 = cos(currNeighborPointing.theta);
    vec3 v2 = vec3(x2, y2, z2);

    return atan2(crossprod(v1, v2).Length(), dotprod(v1, v2));
}



