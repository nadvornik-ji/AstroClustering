/* 
 * File:   HealPixHelper.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 8, 2015, 6:42 PM
 */

#ifndef HEALPIXHELPER_H
#define	HEALPIXHELPER_H

#include <memory>
#include "../model/Coordinate.h"

using namespace std;

class HealPixHelper {
public:
    /**
     * Returns angular distance of two Coordinate structures.
     * @param currObs
     * @param currNeighbor
     * @return Angular distance in double precision
     */
    static double computeDistance(const Coordinate & currObs,
            const Coordinate & currNeighbor);
};

#endif	/* HEALPIXHELPER_H */

