/*
 * File:   ClusteringTypes.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 9, 2015, 4:26 AM
 */

#ifndef CLUSTERINGTYPES_H
#define	CLUSTERINGTYPES_H
#include <map>
#include <set>
#include <unordered_map>
#include <vector>
#include "../model/Coordinate.h"

using namespace std;

typedef vector<Coordinate> coords_list;
typedef vector<Coordinate*> coords_pointer_list;
typedef map<int64, coords_pointer_list> coords_by_pix;
typedef unordered_map<Coordinate *, coords_pointer_list> cluster_map;
typedef unordered_map<Coordinate *, set<Coordinate *>*> cluster_distinct_map;

typedef coords_pointer_list::iterator coords_list_it;
typedef coords_pointer_list::const_iterator coords_list_const_it;
typedef coords_by_pix::iterator coords_by_pix_it;
typedef coords_by_pix::const_iterator coords_by_pix_const_it;
typedef cluster_map::iterator cluster_map_it;
typedef cluster_map::const_iterator cluster_map_const_it;
typedef cluster_distinct_map::iterator cluster_distinct_map_it;
typedef cluster_distinct_map::const_iterator cluster_distinct_map_const_it;

typedef map<int64, coords_pointer_list*> task_index;
typedef task_index::iterator task_index_it;
typedef task_index::const_iterator task_index_const_it;

#endif	/* CLUSTERINGTYPES_H */

