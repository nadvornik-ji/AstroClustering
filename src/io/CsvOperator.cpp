/* 
 * File:   CsvOperator.cpp
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 5, 2015, 6:15 AM
 */

#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>


#include "CsvOperator.h"
#include "../Config.h"
#include "healpix_base.h"

using namespace std;

coords_list *CsvOperator::readCoordinates() {
    inputFile = fopen(inputPath.c_str(), "r");
    if (inputFile == NULL) {
        cerr << "Could not open input file" << endl;
        exit(1);
    }
    coords_list*result = new coords_list();
    
    double ra, dec;
    int imageID, starID;
    char buffer[100];
    fgets(buffer, 100, inputFile); // skip header
#ifdef STDOUT_VERBOSE
    int i = 1;
#endif
    while (fscanf(inputFile, "%lf,%lf,%d,%d\n", &ra, &dec, &imageID, &starID) != EOF) {
#ifdef STDOUT_VERBOSE
        if (i++ % 100000 == 0) {
            cout << "Read rows: " << setw(8) << i << endl;
        }
#endif
        result->push_back(Coordinate(ra, dec, imageID, starID));
    }
    fclose(inputFile);
    result->shrink_to_fit();
    return result;
}

void CsvOperator::writeCatalog(const cluster_distinct_map &result) {
    catalogFile = fopen(catalogOutputPath.c_str(), "w");
    Healpix_Base2 base1(Config::IDNside, NEST, SET_NSIDE);
    fprintf(catalogFile, "cat_id,raj2000,dej2000\n");
#ifdef STDOUT_VERBOSE
    int i = 0;
#endif
    for (cluster_distinct_map_const_it it = result.begin(); it != result.end(); it++) {
        Coordinate * clusterCoords = it->first;
        double ra = clusterCoords->ra;
        double dec = clusterCoords->dec;
        int64 clusterHealPixID = base1.ang2pix(clusterCoords->getHealPixPointing());

        fprintf(catalogFile, "%ld,%0.16lf,%0.16lf\n", clusterHealPixID, ra, dec);
#ifdef STDOUT_VERBOSE
        if (i++ % 100000 == 0) {
            cout << "Wrote catalog rows: " << setw(8) << i << endl;
        }
#endif
    }
    fclose(catalogFile);
}

void CsvOperator::writeRelations(const cluster_distinct_map &result) {
    relationsFile = fopen(relationsOutputPath.c_str(), "w");
    Healpix_Base2 base1(Config::IDNside, NEST, SET_NSIDE);
    

#ifdef STDOUT_VERBOSE
    int i = 0;
    fprintf(relationsFile, "cat_id,raj2000,dej2000,obsname_id,starNo\n");
#else
    fprintf(relationsFile, "cat_id,obsname_id,starNo\n");
#endif

    for (cluster_distinct_map_const_it it = result.begin(); it != result.end(); it++) {
        Coordinate * clusterCoords = it->first;
        int64 clusterHealPixID = base1.ang2pix(clusterCoords->getHealPixPointing());

        set<Coordinate *>::iterator member_it;
        for (member_it = it->second->begin(); member_it != it->second->end(); member_it++) {
            int imageID = (*member_it)->imageID;
            int starID = (*member_it)->starID;
#ifdef STDOUT_VERBOSE           
            double ra = (*member_it)->ra;
            double dec = (*member_it)->dec;
            fprintf(relationsFile, "%ld,%0.16lf,%0.16lf,%d,%d\n", clusterHealPixID,
                    ra, dec, imageID, starID);
#else
            fprintf(relationsFile, "%ld,%d,%d\n", clusterHealPixID, imageID, starID);
#endif

#ifdef STDOUT_VERBOSE
            if (i++ % 100000 == 0) {
                cout << "Wrote relation rows: " << setw(8) << i << endl;
            }
#endif
        }
    }
    fclose(relationsFile);
}