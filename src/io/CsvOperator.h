/* 
 * File:   CsvOperator.h
 * Author: Jiri Nadvornik <nadvornik.ji@gmail.com>
 *
 * Created on April 5, 2015, 6:14 AM
 */

#ifndef READ_CSV_H
#define READ_CSV_H
#include <memory>

#include "../core/ChunkOperator.h"
#include "../helpers/ClusteringTypes.h"

using namespace std;

/**
 * Class responsible for IO from our CSV files. This can be input, catalog 
 * output and relations output files.
 * 
 * @param inputPath
 * @param catalogPath
 * @param relationsPath
 */
class CsvOperator {
public:

    CsvOperator(string inputPath, string catalogPath, string relationsPath) {
        this->inputPath = inputPath;
        this->catalogOutputPath = catalogPath;
        this->relationsOutputPath = relationsPath;
    }
    
    /**
     * Reads the coordinates from the inputPath and returns the parsed list of 
     * coordinates. Has better performance than storing only pointers to those.
     * @return The list of Coordinate structures.
     */
    coords_list *readCoordinates();
    
    /**
     * Takes the result of the clustering and writes the new catalog coordinates
     * to its CSV file.
     * @param result List of clusters
     */
    void writeCatalog(const cluster_distinct_map &result);
    
    /**
     * Takes the result of the clustering and writes the new catalog coordinates
     * to its CSV file.
     * @param result List of clusters
     */
    void writeRelations(const cluster_distinct_map &result);
    
private:
    string inputPath;
    string catalogOutputPath;
    string relationsOutputPath;
    FILE *inputFile;
    FILE *catalogFile;
    FILE *relationsFile;
};

#endif

