# Astro-clustering
This is an application that creates a catalog of objects from list of observations (CSV formatted raj2000, dej2000, imageID, pointIdxOnImage).

This application is still under development, so please consider this an unstable development version!

## Requirements

### Debian

```
apt-get install libcfitsio3-dev build-essential liblapack-dev libboost-chrono1.49 libboost-system1.49 libboost-program-options-dev
```

## Build

1. Get a [HEALPix](http://sourceforge.net/projects/healpix/) C++ library
2. configure it for c++ library -> basic_gcc
3. make it 
4. alter the HEALPIXPATH variable accordingly to point to the folder with basic_gcc includes and libs
5. Get a [Armadillo](http://sourceforge.net/projects/arma/files/armadillo-5.000.1.tar.gz)
6. just extract it and alter the ARMADILLOPATH accordingly - no need to compile or install
7. `make` in the project root folder

##Usage

You can use the large_sample.csv in data folder as follows.

```
./cluster -s incremental -i data/large_sample.csv -c data/large_catalog.csv -r data/large_relations.csv
```

Parameters: 
-s = streaming strategy (possible values: incremental, kmeans, em)
data/large_sample.csv = inputFile
data/large_catalog.csv = catalog File
data/large_relations.csv = catalog assignments file

other (optional) parameters can be found by calling
```
./cluster -h
```