#!/bin/sh
DATA_DIR="/mnt/share/nadvoji1/extract_jan15_csv"
RESULT_DIR="/mnt/share/nadvoji1/results"
LOGS_DIR="/mnt/share/nadvoji1/logs"
STRATEGY="kmeans-lloyd"

for data_size in `seq 6 8`
	do memory=$((12*(10**($data_size-5))))
	for no_threads in 1 2 4 8
		do for task_size in `seq 12 16` 
			do  qsub run_job.pbs -v params="-s $STRATEGY --task-size "$task_size" --no-threads "$no_threads" -i $DATA_DIR/extract_jan15_1e"$data_size".csv -c $RESULT_DIR/extract_jan15_1e"$data_size"_"$STRATEGY"_task_size_"$task_size"_no_threads_"$no_threads"_catalog.csv -r $RESULT_DIR/extract_jan15_1e"$data_size"_"$STRATEGY"_task_size_"$task_size"_no_threads_"$no_threads"_relations.csv --time-log-path $LOGS_DIR/time_extract_jan15_1e"$data_size"_"$STRATEGY"_task_size_"$task_size"_no_threads_"$no_threads".log" -o $LOGS_DIR/output_extract_jan15_1e"$data_size"_"$STRATEGY"_task_size_"$task_size"_no_threads_"$no_threads".log -l nodes=1:ppn=$no_threads -l pmem=${memory}m
		done
	done
done
